% Demo for Edge Boxes (please see readme.txt first).

%% load pre-trained edge detection model and set opts (see edgesDemo.m)
model=load('models/forest/modelBsds'); model=model.model;
model.opts.multiscale=0; model.opts.sharpen=2; model.opts.nThreads=4;

%% set up opts for edgeBoxes (see edgeBoxes.m)
opts = edgeBoxes;
opts.alpha = .65;     % step size of sliding window search
opts.beta  = .75;     % nms threshold for object proposals
opts.minScore = .01;  % min score of boxes to detect
opts.maxBoxes = 1e4;  % max number of boxes to detect

%% paramerter
dataset='oxford5k';
wt=[4 200];
workingpath = '/Users/apple/Documents/SC/nii-kaori-2015/experiments/'
imageformat = 'jpg'
nameContainAllFrame = 'image_test/'
queryImage='';

%% load 'C','nData','wt' from image cluster starting from query image
load([name 'queryClusters_' query '.mat']);

%% extract edgeboxes of query image
query=imread(queryImage);
queryBBS=edgeBoxes(query,model,opts);
% 6th column: 1 (display) vs 0 (no display)
queryBBS(:,6)=0;
% 7th column: number of shared visual words
queryBBS(:,7)=0;

%% load 'E', 'F' of graph starting from the query image
% graph -> shared vsw between image pair + edgeboxes -> count of shared vsw
% in each box
load([dataset '_subgraph/queryGraph' queryImage '.mat']);
for i=1:size(queryClusters,1)
    for j=1:size(queryClusters,2)
        sharedVSW=F(nData,j);
        % how to trace back from visual word ids to their corresponding
        % descriptor locations???
        for k=1:size(queryBBS,1)
            if vswX>=queryBBS(k,1) && vswY>=queryBSS(k,2) && vswX<=(queryBBS(k,1)+queryBBS(k,3)) && vswY<=(queryBSS(k,2)+queryBBS(k,4))
                queryBBS(k,7)=queryBBS(k,7)+1;
            end
        end
    end
end

%% Choose to 20 boxes have the most shared vsw
[Y,I]=sort(queryBBS(:,7));
queryBBS=queryBBS(I,:);
queryBBS=queryBBS(1:20,:);

%% display boxes on query image
figure(1);
imshow(query);
queryBBS2=queryBBS;
queryBBS2(:,5)=1;
queryBBS(:,6)=1;
bbGt('showRes',query,queryBBS2,queryBBS);
