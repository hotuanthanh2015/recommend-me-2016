%  Author: ngocntb
%  Last modified: 21/04/2016

clear;
P = importdata('projection_matrix_32bits.mat');% load projection matrix
thres = importdata('thres_1000000_32bits.mat');% load median matrix

%% feature projection and binarization
numv=1;                 % count number of visual words
nData=5063;             % 5063 images
dataset='oxford5k';     % name of data set
% 
% nData=438;
% dataset='MQA';

% load sifts + visual words
load([dataset '_data.mat'],'img_features');
% because of large dataset, we run on multi-computers
for i=4814:nData
    % load visual words + their neighbor, 1 file = 1 image -> variable vsw
    load([dataset '_Feature/' dataset sprintf('%04d',i) '.mat']);
    % quantization: P vs thes: same #rows
    % img_features vs thres: same #columns
    c=P*img_features(:,numv:numv+length(vsw)-1) - thres(:,vsw');
    numv=numv+length(vsw);
    % normalize: >0 -> 1; <0 -> 0
    c(c > 0) = 1;
    c(c <= 0 ) = 0;
    c=c';
    % save to featureHE file
    save([dataset '_FeatureHE/' dataset sprintf('%06d',i) '.mat'],'vsw','v','xy','s','c');
    clear c;
end



    