%  Author: ngocntb
%  Last modified: 21/04/2016

%% Summary
% d=128 
% -> random dxd matrix M 
% -> invoke function qr() 
% -> get Q: unitary matrix dxd so that A=Q*R 
% -> get first d_b=32 rows of Q -> P: 32*128
% -> consider P as projection matrix
% -> save to file "projection"

% he_data = P*image_features

% -> save median_matrix (32*1000000) to file "thres"
%% generate projection matrix
d = 128; % feature dimension
d_b = 32; % target binary feature dimension
if ~exist('projection_matrix_32bits.mat')
    %%%%% generate projection matrix
    M = randn(d);   % random dxd matrix
    % a complex square matrix U is unitary if its conjugate transpose is also its inverse
    % conjugate transpose of A = transpose of A & complex conjugate of each
    % entry (negating their imaginary parts but not their real parts)
    % => [Q,R] = qr(A), where A is m-by-n, produces an m-by-n upper triangular matrix R and an m-by-m unitary matrix Q so that A = Q*R.
    % orthogonal matrix: QQ'=I; Q' is transpose of Q
    % => used for "linear least squares is an approach fitting a mathematical or statistical model to data in cases where the idealized value provided by the model for any data point is expressed linearly in terms of the unknown parameters of the model."
    % => find a line for random values of square matrix M???
    [Q, R] = qr(M);
    % 128 rows -> just get first 32 rows
    P = Q(1:d_b, :);
    % save P to file
    save(['projection_matrix_' num2str(d_b) 'bits.mat'],'P');
else
    % load P from an existing file
    P = importdata(['projection_matrix_' num2str(d_b) 'bits.mat']);% load projection matrix
    % get first 32 rows
    P = P(1:d_b, :);
end

%% load training data
load('oxfor5k_data.mat');
ndata = 24464227;%size(img_features, 1);


% load('MQA_data.mat');
% ndata = 852829;

% P: 32*128 -> img_features: 128*C -> he_data: 32*C??? C = 1.000.000?
he_data = P*img_features; % data projection

%codebook
%%MQA
%centres = hdf5read('Clustering_l2_10000_852829_128_50it.hdf5','/clusters');

%oxford5k
% load all visual words -> each image uses them to quantize their sifts
centres = hdf5read('Clustering_l2_1000000_24464227_128_50it.hdf5','/clusters');

%% calculate Hamming Embedding threshold
k = 1000000; % codebook size: 1 million
median_matrix = zeros(d_b, k);% learned median matrix -> zero values of d_b rows and k columns
disp('calculate Hamming Embedding threshold');
% 1021 2017 2660 2694 2922
 %matlabpool('12');
    tic
for i = 1 : k   % 1 million
    pos = find(vsw == i); % find elements in vsw where []==i
    if isempty(pos) % can not find
        %median_matrix(:, i) = centres(:, i);
        disp(i);
    else
        temp_matrix = he_data(:, pos); % extract corresponding positions in he_data
        % a: vector -> median(a): value in a and near the average
        % a: 2D matrix -> median(a): median of each column
        % median(a,d): values of d-th dimension are in separate rows, find
        % median
        % median(temp_matrix,2): find the median of each row
        % fill a column at each loop
        median_matrix(:, i) = median(temp_matrix, 2);
    end
end
 toc    
     
%matlabpool close
save(['thres_' num2str(k) '_' num2str(d_b) 'bits.mat'], 'median_matrix');
   