function  graph_construction6_query( dataset,nData, query)
%  Auhtor: thanhht
%  Last modified: 07/06/2016
% them HE vao
% store shared vsw between image pair in each graph

%features F= {[vsw],[xy],[s],[c],[V]}
ht=6;

%==============
%htt-Load HE after pre-computing
data = load('sHE.mat');
%htt-Get normalized values
sHEnorm = data.sHEnorm;
tic
img1=query;
%htth-If graph file does not exist
if ~exist([dataset '_subgraph/graph' img1 '.mat'])
    %htt-Load the visual word file
    %htt-vsw: visual word, c: he; v: neighboring points
    f1= load([dataset '_Feature/' dataset img1 '.mat'],'vsw','c','v');
    %htt-Create a zero 1 row x nData column array
    E=zeros(1,nData);
    F=zeros(1,nData); % recommend-me
    %htt-Display image
    disp(img1);
    %htt-For each image is after ith image
    for img2= 1: nData %img j
        %htt-Load visual word file of jth image
        f2= load([dataset '_Feature/' dataset sprintf('%06d',img2) '.mat'],'vsw','c','v');
        %htt-Check whether they shares same visual words
        sameVS=intersect(f1.vsw,f2.vsw);
        
        %htt-For each emelement in list of same visual words
        for i=1: length( sameVS) % for each same vsw vi
            %htt-Find indices of same vw in ith image
            i1=find( f1.vsw==sameVS(i));
            %htt-Find indices of same vw in jth image
            i2=find( f2.vsw==sameVS(i));
            
            %htt-For each elements in i1 (list of indices of same
            %vsw found in ith image)
            for j1=1:length(i1)
                %htt-bsxfun: apply element-to-element binary
                %operation to 2 ways
                %htt-Count number of hamming code pairs that are
                %different each other
                ham_d = sum(bsxfun(@ne, f1.c(i1(j1),:),f2.c(i2,:))');
                
                %htt-Indices of less-than-threshold distances
                idh=find(ham_d<ht);
                %htt-List of vsw whose distances to f1.c(i1(j1),:)
                %is less than threshold ht
                i22=i2(idh);
                for j2=1:length(i22)
                    %htt-Compute the jaccard similarity
                    %htt-Sort list of neighbors
                    v1=sort(f1.v(i1(j1),:));
                    %htt-Sort list of neighbors
                    v2=sort(f2.v(i22(j2),:));
                    
                    %htt-If v1(i)==v(i+1), remove dup in v1
                    v1(find(v1((1:end-1)')==v1((2:end)')))=[];
                    %htt-Remove dup in v2
                    v2(find(v2((1:end-1)')==v2((2:end)')))=[];
                    
                    %htt-Merge elements of 2 sets v1, v2
                    v3=sort([v1 v2]);
                    %htt-Remove dup in v3
                    v3(find(v3((1:end-1)')==v3((2:end)')))=[];
                    v3=length(v3);
                    sv=length(v1)+length(v2) -v3;
                    
                    if sv>1
                        E(img2)= E(img2)+ sHEnorm(ham_d(idh(j2))+1)+ sv/v3;
                        F(img2)=[f1 f2]; % recommend-me
                    end
                    %E(img2)= E(img2)+ sHEnorm(ham_d(idh(j2))+1)+ sNR(f1.v(i1(j1),:), f2.v(i22(j2),:)); % compute Jaccard similarity
                    %s_NR=sNRfreq(F{img1,5}(i1(j1),:), F{img2,5}(i2(j2),:)); % compute Jaccard similarity
                end
            end
        end
    end
    %htt-A subgraph for an image, for parallel purpose
    savesubgraph(img1,E, F, dataset); % recommend-me
end
toc

end

function savesubgraph(imgFile,E, F, dataset)
save([dataset '_subgraph/queryGraph' imgFile '.mat'],'E', 'F');
end



