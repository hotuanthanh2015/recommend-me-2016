function sHE( )
% similarity score of Hamming Embedding (HE) codes
%  Auhtor: ngocntb
%  Last modified: 18/03/2016

% length of HE code
l=32;

sumCF=[];
temp=0;
sHEnorm=[];
for i=0:l
    temp=temp + nchoosek(l,i);
    disp(nchoosek(l,i));
    sumCF=[sumCF; temp];
    %htt-Formula in 4th page
    sHEnorm=[sHEnorm; -log2(2^-l * temp)/l];
end

save('sHE.mat','sHEnorm','sumCF');

end

