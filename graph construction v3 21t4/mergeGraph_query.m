% Author: ngocntb
% Last modified: 4/04/2016
% Merge all subgraph into final graph
query='';
name='oxford5k';
nData=5063;
% name='MQA';
% nData=438;

% temp edge value array
P=zeros(nData+1,nData+1);
% temp shared vsw array
Q==zeros(nData+1,nData+1);
P=[];
Q=[];

for i=1:nData
    disp([name '_subgraph/graph' sprintf('%06d',i) '.mat']);
    load([name '_subgraph/graph' sprintf('%06d',i) '.mat']);
    P=[P;E];
    Q=[Q;F];
end
E=P;
F=Q;
for i=1:nData
    for j=i+1:nData
        E(j,i)=E(i,j);
        F(j,i)=F(i,j);
    end
end

disp([name '_subgraph/graph' query '.mat']);
load([name '_subgraph/graph' query '.mat']);

% Load edges start from query vertex
for j=1:nData
    P(j,nData+1)=E(j);
end

for j=1:nData
    P(nData+1,j)=P(j,nData+1);
end
E=P;

% Load shared vsw start from query vertex
for j=1:nData
    Q(j,nData+1)=F(j);
end

for j=1:nData
    Q(nData+1,j)=F(j,nData+1);
end
F=Q;

nData=nData+1;
save([name '_finalQueryGraph.mat'],'E','F','nData');