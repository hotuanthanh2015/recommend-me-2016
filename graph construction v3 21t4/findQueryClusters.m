function findQueryClusters(queryFile)
% Auhtor: thanhht
% Last modified: 07/06/2016
% GBFS - greedy breadth-first search
% image clusters & a query image

name='oxford5k';
wt=[4 200];
queryClusters=[];
% load instance graph (including a vertex of the query)
load([name '_finalQueryGraph.mat']);

% load image clusters ->  'C', 'nData', 'wt'
load([name '_instanceClustering1_' num2str(wt(1)) '_' num2str(wt(2)) '.mat']);

tic
for i=1:nData
    ctemp=C{i};
    totalWt=0;
    for j=1:size(ctemp,1)
        totalWt=totalWt+E(j,nData+1);
    end
    if totalWt<size(ctemp,1)>wt(2)
        queryClusters=[queryClusters, ctemp];
    end
end
save([name 'queryClusters_' query '.mat'],'queryClusters');
toc

end

