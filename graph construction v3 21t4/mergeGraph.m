%  Auhtor: ngocntb
%  Last modified: 4/04/2016
%Merge all subgraph into final graph
name='oxford5k';
nData=5063;
% name='MQA';
% nData=438;

P=zeros(nData,nData);
endi=0;
P=[];

for i=1:nData 
    disp([name '_subgraph/graph' sprintf('%06d',i) '.mat']);
    load([name '_subgraph/graph' sprintf('%06d',i) '.mat']);
    P=[P;E];
end
E=P;
for i=1:nData 
    for j=i+1:nData 
        E(j,i)=E(i,j);
    end
end
save([name '_finalGraph.mat'],'E','nData');