function all=performance4(name, type,wt1,wt2)
    %  Auhtor: ngocntb
    %  Last modified: 8/04/2016
    %  name: dataset
    %  type: 1, 2

    load([name '_instanceClustering' type '_' num2str(wt1) '_' num2str(wt2) '.mat']);
    load([name '_groundtruth.mat']);

    % Ppair, Rpair, Fpair
        
    pDR=[];
    pGT=[];
    %htt-#images
    for i=1:nData
        disp(i);
        if length(C{i})> 1 
            ctemp=sort(C{i});
            pDR=[pDR; nchoosek(ctemp,2)]; 
        end
    end
    
    for j=1: nCluster  
        gttemp=sort(GT{j});
        pGT=[pGT; nchoosek(gttemp,2)];
    end 
    
    pDR=unique(pDR,'rows');
    pGT=unique(pGT,'rows');
    
    pSame= intersect(pDR,pGT,'rows');
    
    sPair=length(pSame);
    sDR=length(pDR);
    sGT=length(pGT);    
    
    pPair=sPair/sDR;
    rPair=sPair/sGT;
    fPair=2*pPair* rPair/(pPair+ rPair);

    all=[ mean(fPair), mean(pPair), mean(rPair)];
    save([name '_result' type '_p4_' num2str(wt1) '_' num2str(wt2) '.mat'],'pPair', 'rPair', 'fPair',  'all','nCluster');    

end