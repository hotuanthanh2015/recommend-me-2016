function GBFS(  )
%  Auhtor: ngocntb
%  Last modified: 4/04/2016
%GBFS - greedy breadth-first search
%input instance graph G and an image I

name='oxford5k';
load([name '_finalGraph.mat']);
dmax=2;
wt=[4 200];
C=cell(1,nData);
matlabpool('12');
tic

%htt-Parallel loop
%htt-nData: #images -> #nodes in graph
parfor i =1:nData
    %input I
    disp(i);
    %htt-Init an instance cluster C that contains only one image I
    ctemp=[i];
    %htt-Init an empty queue Q and enqueue I to Q
    Q2=[i];
    %htt-dmax=2
    for j=1: dmax
        Q1=Q2;
        %htt-Dequeue I from Q
        Q2=[];
        for q=1:length(Q1)
            %htt-Find all neighbors
            NI=find(E(Q1(q),:)>0);
            %htt-Remove I from NI
            NI=setdiff(NI,ctemp);
            %htt-Sort by weights desc
            [~,id]=sort(E(Q1(q),NI),'descend');
            %htt-Step 3: average weight test
            %htt-For each image in NI
            NI=NI(id);
            for ni=1:length(NI)
                w=E(NI(ni),ctemp);
                aw=mean(w);
                if aw> wt(j)
                    Q2=[Q2; NI(ni)];
                    ctemp=[ctemp; NI(ni)];
                end
            end            
        end
    end
    C{i}=ctemp;
end
toc
matlabpool close
save([name '_instanceClustering1_' num2str(wt(1)) '_' num2str(wt(2)) '.mat'],'C','nData','wt');
end

