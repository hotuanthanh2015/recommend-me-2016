function  graph_construction5( dataset,nData, starti, endi)
%  Auhtor: ngocntb
%  Last modified: 22/4/2016
% them HE vao


    %features F= {[vsw],[xy],[s],[c],[V]}
    ht=6;
    
    
    %==============    
    data = load('sHE.mat');
    sHEnorm = data.sHEnorm;  
    matlabpool('12');
    tic
    parfor img1=starti:endi
        if ~exist([dataset '_subgraph/graph' sprintf('%06d',img1) '.mat'])
            f1= load([dataset '_Feature/' dataset sprintf('%06d',img1) '.mat'],'vsw','c','v');
            E=zeros(1,nData);
            disp(img1);
            for img2= img1+1: nData %img j     
                f2= load([dataset '_Feature/' dataset sprintf('%06d',img2) '.mat'],'vsw','c','v');
                
                
                sameVS=intersect(f1.vsw,f2.vsw);
                
                for i=1: length( sameVS) % for each same vsw vi 
                    i1=find( f1.vsw==sameVS(i));
                    i2=find( f2.vsw==sameVS(i)); 
                    
                    for j1=1:length(i1)
                        ham_d = sum(bsxfun(@ne, f1.c(i1(j1),:),f2.c(i2,:))');
                        idh=find(ham_d<ht);
                        i22=i2(idh);
                        for j2=1:length(i22)
                            %jaccard similarity
                            v1=sort(f1.v(i1(j1),:));
                            v2=sort(f2.v(i22(j2),:));   
                            
                            v1(find(v1((1:end-1)')==v1((2:end)')))=[];
                            v2(find(v2((1:end-1)')==v2((2:end)')))=[];
                            
                            v3=sort([v1 v2]);
                            v3(find(v3((1:end-1)')==v3((2:end)')))=[];
                            v3=length(v3);
                            sv=length(v1)+length(v2) -v3;
                            
                            if sv>1
                                E(img2)= E(img2)+ sHEnorm(ham_d(idh(j2))+1)+ sv/v3;
                            end
                            %E(img2)= E(img2)+ sHEnorm(ham_d(idh(j2))+1)+ sNR(f1.v(i1(j1),:), f2.v(i22(j2),:)); % compute Jaccard similarity   
                           %s_NR=sNRfreq(F{img1,5}(i1(j1),:), F{img2,5}(i2(j2),:)); % compute Jaccard similarity  
                        end
                    end                
                end
            end
           savesubgraph(img1,E, dataset);
        end
    end
     toc    
     
matlabpool close
end

function savesubgraph(img1,E, dataset)
 save([dataset '_subgraph/graph' sprintf('%06d',img1) '.mat'],'E');
end



